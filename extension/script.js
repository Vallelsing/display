chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        // listen for messages sent from background.js
        if (request.message === 'hello!') {
            console.log(request.url) // new url is now in content scripts!
        }
    });

chrome.runtime.sendMessage("show url", function (result) {
    alert(result);
});

chrome.runtime.sendMessage(
    {
        contentScriptQuery: "getdata",
        //data: {"data":"data"}, 
        url: "https://reqres.in/api/unknown"
    }, function (response) {
        if (response != undefined && response != "") {
            //callback(response);
            console.log(response);
        }
        else {
            console.log(response);
        }
    });


// Request API
// https://stackoverflow.com/questions/53405535/how-to-enable-fetch-post-in-chrome-extension-contentscript
// https://reqres.in/

// Access DOM element
// https://stackoverflow.com/questions/19758028/chrome-extension-get-dom-content
// https://stackoverflow.com/questions/21314897/access-dom-elements-through-chrome-extension

// Fetch API
// https://developer.mozilla.org/en-US/docs/Web/API/fetch