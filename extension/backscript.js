/*
    Get the current tab 
*/
async function getCurrentTab() {
    let queryOptions = { active: true, currentWindow: true };
    let [tab] = await chrome.tabs.query(queryOptions);
    return tab.url;
}

/*
    Check if the current tab is on twitter, if it is, send a request to a API to get data
*/
getCurrentTab().then(function (url) {
    if (url.includes("https://twitter.com")) {
        const url2 = new URL(url);
        let tag = url2.pathname;
        tag = tag.substring(1);
        if (tag.indexOf('/') != -1) {
            tag = tag.substring(0, tag.indexOf('/'));
        }
        console.log(tag);

        let urlAPI = "http://localhost:8080/api/v1/twitter-users/tags/" + tag;
        fetch(urlAPI)
            .then(response => response.json())
            .then(
                response => {                  
                    document.getElementById('fullname').innerText = response.firstname + ' ' + response.lastname;
                    document.getElementById('gender').innerText = response.gender;
                }
                
            )
            .catch(error => console.log(error))
    }
})