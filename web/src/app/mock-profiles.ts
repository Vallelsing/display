import { Profile } from './profile';

export const PROFILES: Profile[] = [
  { id: '0', tag: 'vpecresse', firstname: 'Valerie', lastname: "Pecresse", gender: "F", createdAt:"2022-01-24T18:33:53.942" },
  { id: '1', tag:"EmmanuelMacron", firstname:"Emmanuel", lastname :"Macron", gender :"MALE", createdAt :"2022-01-24T18:33:53.922"}
];