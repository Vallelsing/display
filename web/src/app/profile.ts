export interface Profile {
    id: string;
    tag: string;
    firstname: string;
    lastname: string;
    gender: string;
    createdAt: string;
  }