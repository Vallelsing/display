import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MessageService } from '../message.service';

import { Profile } from '../profile';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {

  profiles: Profile[] = [];
  profile: Profile = this.profileService.reset();

  constructor(
    private profileService: ProfileService,
    private http: HttpClient,
    private messageService: MessageService) {}

  ngOnInit(): void {
    this.getProfiles();
  }
  
  getProfiles(): void {
    this.profileService.getProfiles()
      .subscribe(profiles => this.profiles = profiles);
  }

  createProfileWithObservable(): Observable<Profile> {
    const body = {
      "firstname": this.profile?.firstname,
      "gender": this.profile?.gender,
      "lastname": this.profile?.lastname,
      "tag": this.profile?.tag
    };
    const url = `http://localhost:8080/api/v1/admin/twitter-users`;
    return this.http.post<Profile>(url, body);
  }

  createProfile(): void {
    this.createProfileWithObservable()
      .subscribe(
        profile => {
          this.getProfiles();
          this.profileService.reset();
        }
      );
    this.messageService.add('Profile Added Successfully');
  }

}