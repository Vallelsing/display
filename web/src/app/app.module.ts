import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfilesComponent } from './profiles/profiles.component';

import { FormsModule } from '@angular/forms';
import { MessagesComponent } from './messages/messages.component';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component'; // <-- NgModel lives here

@NgModule({
  declarations: [
    AppComponent,
    ProfilesComponent,
    MessagesComponent,
    ProfileDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
