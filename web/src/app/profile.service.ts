import { Injectable } from '@angular/core';

import { Profile } from './profile';
import { PROFILES } from './mock-profiles';
import { MessageService } from './message.service';

import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  profiles:any = [];

  constructor(private messageService: MessageService, private http: HttpClient) { }

  // Function creating an empty profile
  reset(): Profile {
    return { id: '', tag: '', firstname: '', lastname: "", gender: "", createdAt:"" };
  }

  getProfiles(): Observable<Profile[]> {
    // If you want to get Mock Profiles
    // const mockProfiles = of(PROFILES);

    // Get from DB
    const url ='http://localhost:8080/api/v1/admin/twitter-users'
    this.profiles = this.http.get<Profile[]>(url);

    this.messageService.add('Fetched all profiles');
    return this.profiles;
  }

}
