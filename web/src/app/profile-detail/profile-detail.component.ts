import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { ProfileService } from '../profile.service';
import { Profile } from '../profile';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.component.html',
  styleUrls: ['./profile-detail.component.css']
})
export class ProfileDetailComponent implements OnInit {

  @Input() profile?: Profile;

  constructor(
    private route: ActivatedRoute,
    private profileService: ProfileService,
    private location: Location,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getProfile();
  }
  
  getProfile(): void {
    const id = String(this.route.snapshot.paramMap.get('id'));
    console.log(id);
    this.getProfileById(id)
      .subscribe(profile => this.profile = profile);
  }

  getProfileById(id: String): Observable<Profile> {
    const url = `http://localhost:8080/api/v1/admin/twitter-users/${id}`;
    return this.http.get<Profile>(url);
  }

  modifyWithObservable(): Observable<Profile> {
    const body = {
      "firstname": this.profile?.firstname,
      "gender": this.profile?.gender,
      "lastname": this.profile?.lastname,
      "tag": this.profile?.tag
    };
    const url = `http://localhost:8080/api/v1/admin/twitter-users/${this.profile?.id}`;
    return this.http.put<Profile>(url, body);
  }

  modify(): void {
    this.modifyWithObservable()
      .subscribe(
        profile => {
          this.profileService.getProfiles();
          this.router.navigate(['/profiles']);
        }
      );
  }
  
  deleteWithObservable(): Observable<Profile> {
    const url = `http://localhost:8080/api/v1/admin/twitter-users/${this.profile?.id}`;
    return this.http.delete<Profile>(url);
  }

  delete(): void {
    this.deleteWithObservable()
      .subscribe(
        profile => {
          this.profileService.getProfiles();
          profile = this.profileService.reset();
          this.router.navigate(['/profiles']);
        }
      );
  }

  goBack(): void {
    this.location.back();
  }

}
