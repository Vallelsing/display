package org.tse.poc.vnbl.display.dao;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.tse.poc.vnbl.display.model.TwitterUser;
import org.tse.poc.vnbl.display.util.Gender;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
class TwitterUserRepositoryTest {

    @Autowired
    private TwitterUserRepository twitterUserRepository;

    @Test
    void findTwitterUserByTag() {
        TwitterUser francoisHollande = twitterUserRepository.findByTag("fhollande").orElse(null);

        assertNotNull(francoisHollande);
        assertEquals("François", francoisHollande.getFirstname());
        assertEquals("Hollande", francoisHollande.getLastname());
        assertEquals(Gender.MALE, francoisHollande.getGender());
    }

    @Test
    void insertAndDeleteById() {
        TwitterUser ericZemmour = new TwitterUser(
                "ZemmourEric",
                "Eric",
                "Zemmour",
                Gender.MALE,
                LocalDateTime.now()
        );

        twitterUserRepository.insert(ericZemmour);
        TwitterUser zemmourEric = twitterUserRepository.findByTag("ZemmourEric").orElse(null);
        assertNotNull(zemmourEric);

        twitterUserRepository.deleteById(zemmourEric.getId());
        TwitterUser zemmourDelete = twitterUserRepository.findByTag("ZemmourEric").orElse(null);
        assertNull(zemmourDelete);
    }
}