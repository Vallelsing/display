package org.tse.poc.vnbl.display.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.tse.poc.vnbl.display.dao.TwitterUserRepository;
import org.tse.poc.vnbl.display.model.TwitterUser;
import org.tse.poc.vnbl.display.util.Gender;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.is;

class TwitterUserPublicControllerTest extends ControllerBaseTest{

    @Autowired
    private TwitterUserRepository twitterUserRepository;

    @Test
    void fetchAllTwitterUsersWithoutId() throws Exception{
        mvc.perform(get("/twitter-users").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(3)))
                .andExpect(jsonPath("$.[0].length()", is(5)))
                .andExpect(jsonPath("$[0].tag", is("EmmanuelMacron")))
                .andExpect(jsonPath("$[0].firstname", is("Emmanuel")))
                .andExpect(jsonPath("$[0].lastname", is("Macron")))
                .andExpect(jsonPath("$[0].gender", is(Gender.MALE.toString())));
    }

    @Test
    void fetchTwitterUserByTag() throws Exception{
        mvc.perform(get("/twitter-users/tags/vpecresse").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(5)))
                .andExpect(jsonPath("$.tag", is("vpecresse")))
                .andExpect(jsonPath("$.firstname", is("Valérie")))
                .andExpect(jsonPath("$.lastname", is("Pécresse")))
                .andExpect(jsonPath("$.gender", is(Gender.FEMALE.toString())));
    }
}