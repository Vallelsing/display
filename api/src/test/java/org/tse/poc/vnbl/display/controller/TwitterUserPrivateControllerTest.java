package org.tse.poc.vnbl.display.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.tse.poc.vnbl.display.dao.TwitterUserRepository;
import org.tse.poc.vnbl.display.model.TwitterUser;
import org.tse.poc.vnbl.display.model.dto.input.TwitterUserDtoIn;
import org.tse.poc.vnbl.display.util.Gender;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TwitterUserPrivateControllerTest extends ControllerBaseTest{

    @Autowired
    private TwitterUserRepository twitterUserRepository;

    @Test
    @Order(1)
    void fetchAllTwitterUsersWithId() throws Exception{
        mvc.perform(get("/admin/twitter-users").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(3)))
                .andExpect(jsonPath("$.[0].length()", is(6)))
                .andExpect(jsonPath("$[0].tag", is("EmmanuelMacron")))
                .andExpect(jsonPath("$[0].firstname", is("Emmanuel")))
                .andExpect(jsonPath("$[0].lastname", is("Macron")))
                .andExpect(jsonPath("$[0].gender", is(Gender.MALE.toString())));
    }

    @Test
    @Order(2)
    void fetchTwitterUserById() throws Exception{

        TwitterUser hollande = twitterUserRepository.findByTag("fhollande").orElse(null);

        assert hollande != null;
        mvc.perform(get("/admin/twitter-users/" + hollande.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(6)))
                .andExpect(jsonPath("$.id", is(hollande.getId())))
                .andExpect(jsonPath("$.tag", is("fhollande")))
                .andExpect(jsonPath("$.firstname", is("François")))
                .andExpect(jsonPath("$.lastname", is("Hollande")))
                .andExpect(jsonPath("$.gender", is(Gender.MALE.toString())));
    }

    @Test
    @Order(3)
    void createTwitterUser() throws Exception{
        // Create the TwitterUserDtoIn that will be post to the API
        TwitterUserDtoIn twitterUserDtoIn = new TwitterUserDtoIn();
        twitterUserDtoIn.setTag("MLP_officiel");
        twitterUserDtoIn.setFirstname("Marine");
        twitterUserDtoIn.setLastname("Le Pen");
        twitterUserDtoIn.setGender(Gender.FEMALE);

        // Send the POST request
        ObjectMapper mapper = new ObjectMapper();
        byte[] twitterUserDtoInAsBytes = mapper.writeValueAsBytes(twitterUserDtoIn);

        mvc.perform(post("/admin/twitter-users")
                .contentType(MediaType.APPLICATION_JSON).content(twitterUserDtoInAsBytes))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));


        List<TwitterUser> usersList = twitterUserRepository.findAll();
        assertEquals(4, usersList.size());
        assertEquals("MLP_officiel", usersList.get(3).getTag());
        assertEquals("Marine", usersList.get(3).getFirstname());
        assertEquals("Le Pen", usersList.get(3).getLastname());
        assertEquals(Gender.FEMALE, usersList.get(3).getGender());
    }

    @Test
    @Order(4)
    void editTwitterUser() throws Exception{
        TwitterUser marine = twitterUserRepository.findByTag("MLP_officiel").orElse(null);
        assertNotNull(marine);

        // Create the TwitterUserDtoIn that will be put to the API
        TwitterUserDtoIn twitterUserDtoIn = new TwitterUserDtoIn();
        twitterUserDtoIn.setTag("MLP_officiel");
        twitterUserDtoIn.setFirstname("Marine");
        twitterUserDtoIn.setLastname("Le Pen");
        twitterUserDtoIn.setGender(Gender.MALE);

        // Send the PUT request
        ObjectMapper mapper = new ObjectMapper();
        byte[] twitterUserDtoInAsBytes = mapper.writeValueAsBytes(twitterUserDtoIn);


        mvc.perform(put("/admin/twitter-users/" + marine.getId())
                .contentType(MediaType.APPLICATION_JSON).content(twitterUserDtoInAsBytes))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        marine = twitterUserRepository.findByTag("MLP_officiel").orElse(null);
        assertNotNull(marine);
        assertEquals(Gender.MALE, marine.getGender());
    }

    @Test
    @Order(5)
    void deleteTwitterUser() throws Exception {
        TwitterUser marine = twitterUserRepository.findByTag("MLP_officiel").orElse(null);
        assertNotNull(marine);

        mvc.perform(delete("/admin/twitter-users/" + marine.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<TwitterUser> usersList = twitterUserRepository.findAll();
        assertEquals(3, usersList.size());
    }
}