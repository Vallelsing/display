package org.tse.poc.vnbl.display.service;

import org.tse.poc.vnbl.display.model.TwitterUser;

import java.util.List;
import java.util.Optional;

public interface TwitterUserService {

    List<TwitterUser> getAllTwitterUsers();
    Optional<TwitterUser> findTwitterUserByTag(String tag);
    Optional<TwitterUser> findTwitterUserById(String id);
    TwitterUser insertTwitterUser(TwitterUser user);
    TwitterUser saveTwitterUser(TwitterUser user);
    void deleteTwitterUserById(String id);
}
