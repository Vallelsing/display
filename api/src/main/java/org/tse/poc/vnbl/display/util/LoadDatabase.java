package org.tse.poc.vnbl.display.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.tse.poc.vnbl.display.dao.TwitterUserRepository;
import org.tse.poc.vnbl.display.model.TwitterUser;

import java.time.LocalDateTime;

@Configuration
@Slf4j
public class LoadDatabase {

    @Bean
    @Profile("test")
    CommandLineRunner initTestDatabase(TwitterUserRepository twitterUserRepository) {
        return args -> {
            log.info("Database seeding started for tests...");
            // Delete all document in the collections to have a clean database for tests
            twitterUserRepository.deleteAll();

            log.info("Database has been cleaned from previous entries, now seeding...");

            TwitterUser emmanuelMacron = new TwitterUser(
                    "EmmanuelMacron",
                    "Emmanuel",
                    "Macron",
                    Gender.MALE,
                    LocalDateTime.now()
            );
            TwitterUser francoisHollande = new TwitterUser(
                    "fhollande",
                    "François",
                    "Hollande",
                    Gender.MALE,
                    LocalDateTime.now()
            );
            TwitterUser valeriePecresse = new TwitterUser(
                    "vpecresse",
                    "Valérie",
                    "Pécresse",
                    Gender.FEMALE,
                    LocalDateTime.now()
            );

            twitterUserRepository.insert(emmanuelMacron);
            twitterUserRepository.insert(francoisHollande);
            twitterUserRepository.insert(valeriePecresse);

            log.info("Database ready for tests!");
        };
    }
    
    @Bean
    @Profile("!test")
    CommandLineRunner initDevDatabase(TwitterUserRepository twitterUserRepository) {
        return args -> {
            log.info("Database seeding started for dev ...");
            // Delete all document in the collections to have a clean database for tests
            twitterUserRepository.deleteAll();

            log.info("Database has been cleaned from previous entries, now seeding...");

            TwitterUser emmanuelMacron = new TwitterUser(
                    "EmmanuelMacron",
                    "Emmanuel",
                    "Macron",
                    Gender.MALE,
                    LocalDateTime.now()
            );
            TwitterUser francoisHollande = new TwitterUser(
                    "fhollande",
                    "François",
                    "Hollande",
                    Gender.MALE,
                    LocalDateTime.now()
            );
            TwitterUser valeriePecresse = new TwitterUser(
                    "vpecresse",
                    "Valérie",
                    "Pécresse",
                    Gender.FEMALE,
                    LocalDateTime.now()
            );

            twitterUserRepository.insert(emmanuelMacron);
            twitterUserRepository.insert(francoisHollande);
            twitterUserRepository.insert(valeriePecresse);

            log.info("Database ready for dev !");
        };
    }

}
