package org.tse.poc.vnbl.display.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.tse.poc.vnbl.display.model.dto.input.TwitterUserDtoIn;
import org.tse.poc.vnbl.display.model.dto.output.TwitterUserDtoOut;
import org.tse.poc.vnbl.display.util.Gender;

import java.time.LocalDateTime;

@Data
@Document
@NoArgsConstructor
public class TwitterUser {
    @Id private String id;

    @Indexed(unique = true)
    private String tag;

    private String firstname;
    private String lastname;
    private Gender gender;
    private LocalDateTime createdAt;

    public TwitterUser(String tag,
                       String firstname,
                       String lastname,
                       Gender gender,
                       LocalDateTime createdAt) {
        this.tag = tag;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.createdAt = createdAt;
    }

    public TwitterUser(TwitterUserDtoIn twitterUserDtoIn) {
        this.tag = twitterUserDtoIn.getTag();
        this.firstname = twitterUserDtoIn.getFirstname();
        this.lastname = twitterUserDtoIn.getLastname();
        this.gender = twitterUserDtoIn.getGender();
        this.createdAt = LocalDateTime.now();
    }

    public TwitterUserDtoOut entityToDtoConversionWithoutId() {
        return new TwitterUserDtoOut(this, false);
    }

    public TwitterUserDtoOut entityToDtoConversionWithId() {
        return new TwitterUserDtoOut(this, true);
    }

    public void editFromDtoIn( TwitterUserDtoIn twitterUserDtoIn) {
        this.tag = twitterUserDtoIn.getTag();
        this.firstname = twitterUserDtoIn.getFirstname();
        this.lastname = twitterUserDtoIn.getLastname();
        this.gender = twitterUserDtoIn.getGender();
    }
}
