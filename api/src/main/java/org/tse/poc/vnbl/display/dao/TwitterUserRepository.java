package org.tse.poc.vnbl.display.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.tse.poc.vnbl.display.model.TwitterUser;

import java.util.Optional;

public interface TwitterUserRepository extends MongoRepository<TwitterUser, String> {
    Optional<TwitterUser> findByTag(String tag);
}
