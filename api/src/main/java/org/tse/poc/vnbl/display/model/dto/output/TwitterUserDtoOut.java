package org.tse.poc.vnbl.display.model.dto.output;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.tse.poc.vnbl.display.model.TwitterUser;
import org.tse.poc.vnbl.display.util.Gender;

import java.time.LocalDateTime;

@Data
public class TwitterUserDtoOut {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;
    private String tag;
    private String firstname;
    private String lastname;
    private Gender gender;
    private LocalDateTime createdAt;

    public TwitterUserDtoOut(TwitterUser twitterUser, boolean withId) {
        if (withId) {
            this.id = twitterUser.getId();
        } else {
            this.id = null;
        }
        this.tag = twitterUser.getTag();
        this.firstname = twitterUser.getFirstname();
        this.lastname = twitterUser.getLastname();
        this.gender = twitterUser.getGender();
        this.createdAt = twitterUser.getCreatedAt();
    }
}
