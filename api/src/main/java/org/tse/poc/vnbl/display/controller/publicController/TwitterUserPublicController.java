package org.tse.poc.vnbl.display.controller.publicController;

import lombok.AllArgsConstructor;

import org.springframework.web.bind.annotation.*;
import org.tse.poc.vnbl.display.model.TwitterUser;
import org.tse.poc.vnbl.display.model.dto.output.TwitterUserDtoOut;
import org.tse.poc.vnbl.display.service.TwitterUserService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("twitter-users")
@AllArgsConstructor
public class TwitterUserPublicController {
    
    private final TwitterUserService twitterUserService;

    @CrossOrigin
    @GetMapping
    public List<TwitterUserDtoOut> fetchAllTwitterUsersWithoutId() {
        List<TwitterUser> twitterUsers = this.twitterUserService.getAllTwitterUsers();
        List<TwitterUserDtoOut> twitterUserDtoOutList = new ArrayList<>();

        twitterUsers.forEach(user -> twitterUserDtoOutList.add(user.entityToDtoConversionWithoutId()));

        return twitterUserDtoOutList;
    }

    @CrossOrigin
    @GetMapping("/tags/{tag}")
    public TwitterUserDtoOut fetchTwitterUserByTag(@PathVariable String tag) {
        TwitterUserDtoOut twitterUserDtoOut = null;
        TwitterUser twitterUser = this.twitterUserService.findTwitterUserByTag(tag).orElse(null);

        if (twitterUser != null) {
            twitterUserDtoOut = twitterUser.entityToDtoConversionWithoutId();
        }

        return twitterUserDtoOut;
    }
}
