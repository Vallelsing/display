package org.tse.poc.vnbl.display.controller.privateController;

import lombok.AllArgsConstructor;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.*;
import org.tse.poc.vnbl.display.model.TwitterUser;
import org.tse.poc.vnbl.display.model.dto.input.TwitterUserDtoIn;
import org.tse.poc.vnbl.display.model.dto.output.TwitterUserDtoOut;
import org.tse.poc.vnbl.display.service.TwitterUserService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("admin/twitter-users")
@AllArgsConstructor
public class TwitterUserPrivateController {

    private final TwitterUserService twitterUserService;

    @CrossOrigin
    @GetMapping
    public List<TwitterUserDtoOut> fetchAllTwitterUsersWithId() {
        List<TwitterUser> twitterUsers = this.twitterUserService.getAllTwitterUsers();
        List<TwitterUserDtoOut> twitterUserDtoOutList = new ArrayList<>();

        twitterUsers.forEach(user -> twitterUserDtoOutList.add(user.entityToDtoConversionWithId()));

        return twitterUserDtoOutList;
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public TwitterUserDtoOut fetchTwitterUserById(@PathVariable String id) {
        TwitterUserDtoOut twitterUserDtoOut = null;
        TwitterUser twitterUser = this.twitterUserService.findTwitterUserById(id).orElse(null);

        if (twitterUser != null) {
            twitterUserDtoOut = twitterUser.entityToDtoConversionWithId();
        }

        return twitterUserDtoOut;
    }

    @CrossOrigin
    @PostMapping()
    public TwitterUserDtoOut createTwitterUser(@Valid @RequestBody TwitterUserDtoIn twitterUserDtoIn){
        TwitterUser twitterUser = twitterUserDtoIn.dtoToEntityConversion();
        twitterUser = this.twitterUserService.insertTwitterUser(twitterUser);
        return twitterUser.entityToDtoConversionWithId();
    }

    @CrossOrigin
    @PutMapping("/{id}")
    public TwitterUserDtoOut editTwitterUserByTag(@PathVariable String id, @Valid @RequestBody TwitterUserDtoIn twitterUserDtoIn){
        TwitterUserDtoOut twitterUserDtoOut = null;
        TwitterUser twitterUser = this.twitterUserService.findTwitterUserById(id).orElse(null);

        if (twitterUser != null) {
            twitterUser.editFromDtoIn(twitterUserDtoIn);
            twitterUser = this.twitterUserService.saveTwitterUser(twitterUser);
            twitterUserDtoOut = twitterUser.entityToDtoConversionWithId();
        }

        return twitterUserDtoOut;
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    public void deleteTwitterUser(@PathVariable String id){
        this.twitterUserService.deleteTwitterUserById(id);
    }
}
