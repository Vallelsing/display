package org.tse.poc.vnbl.display.util;

public enum Gender {
    MALE, FEMALE, UNKNOWN
}
