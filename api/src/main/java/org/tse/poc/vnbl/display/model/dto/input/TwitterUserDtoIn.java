package org.tse.poc.vnbl.display.model.dto.input;

import lombok.Data;
import org.tse.poc.vnbl.display.model.TwitterUser;
import org.tse.poc.vnbl.display.util.Gender;

@Data
public class TwitterUserDtoIn {

    private String tag;
    private String firstname;
    private String lastname;
    private Gender gender;

    public TwitterUser dtoToEntityConversion() {
        return new TwitterUser(this);
    }
}
