package org.tse.poc.vnbl.display.service.implementation;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.tse.poc.vnbl.display.dao.TwitterUserRepository;
import org.tse.poc.vnbl.display.model.TwitterUser;
import org.tse.poc.vnbl.display.service.TwitterUserService;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TwitterUserServiceImplementation implements TwitterUserService {

    private final TwitterUserRepository twitterUserRepository;

    @Override
    public List<TwitterUser> getAllTwitterUsers() {
        return this.twitterUserRepository.findAll();
    }

    @Override
    public Optional<TwitterUser> findTwitterUserByTag(String tag) {
        return this.twitterUserRepository.findByTag(tag);
    }

    @Override
    public Optional<TwitterUser> findTwitterUserById(String id) {
        return this.twitterUserRepository.findById(id);
    }

    @Override
    public TwitterUser insertTwitterUser(TwitterUser user) {
        this.twitterUserRepository.insert(user);
        return user;
    }

    @Override
    public TwitterUser saveTwitterUser(TwitterUser user) {
        this.twitterUserRepository.save(user);
        return user;
    }

    @Override
    public void deleteTwitterUserById(String id) {
        this.twitterUserRepository.findById(id).ifPresent(this.twitterUserRepository::delete);
    }
}
